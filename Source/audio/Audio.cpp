/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    std::cout << "note velo: " << message.getVelocity() << std::endl;
//    sineOscillator.setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
//    sineOscillator.setAmplitude(message.getVelocity()/127.f);
    
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    while(numSamples--)
    {
        *outL = *inL;
        *outR = *inR;
       
        float sample = sineBeep.getSample();
        
        *outL = sample; //   output
        *outR = sample;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sineOscillator.setSampleRate(device->getCurrentSampleRate());
    sineBeep.setSampleRate(device->getCurrentSampleRate());
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::audioDeviceStopped()
{

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/** Controls the amplitude and freq of sineBeep, sending a 100ms 440Hz impulse when called */
void Audio::beep()
{
    DBG("beep");
    
    sineBeep.setAmplitude(1.0);
    sineBeep.setFrequency(440.0);

    
    uint32 time = Time::getMillisecondCounter();
    Time::waitForMillisecondCounter(time + 100);
    
    sineBeep.setAmplitude(0.0);
}

