//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 07/11/2016.
//
//

#ifndef Counter_hpp
#define Counter_hpp

#include "../../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

/** This 'Counter' class performs a counting operation in its own thread, in order to set sequencer tempo */

class Counter : public Thread
{
public:
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Counter::Listener inherits this class */
    class Listener
    {
    public:
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /** Destructor */
        virtual ~Listener() {}
        
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        /** Called when the next counter has reached the next interval */
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Constructor */
    Counter();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Destructor */
    ~Counter();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Sets a counter listener */
    void setListener (Listener* newListener);
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Runs the counter */
    void run() override;
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Starts the run function */
    void startCounterThread();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Stops the run function */
    void stopCounterThread();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Returns state 'true' if thread is running */
    bool isRunning();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Changes the counter time interval to set the tempo in BPM */
    void setTempo (int tempo);
    
private:
    Listener* listener;
    uint32 counterValue;
    uint32 timeInterval;
    
};




#endif /* Counter_h */
