//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 03/11/2016.
//
//

#include "SinOscillator.h"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SinOscillator::SinOscillator() : frequency(440.f), phasePosition(0.f)
{
    //CONSTRUCTOR
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SinOscillator::~SinOscillator()
{
    //DESTRUCTOR
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SinOscillator::setSampleRate(int newSampleRate)
{
    sampleRate = newSampleRate;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SinOscillator::setAmplitude(float newAmplitude)
{
    amplitude = newAmplitude;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SinOscillator::setFrequency(float newFrequency)
{
    frequency = newFrequency;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
float SinOscillator::getSample()
{
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency.get())/sampleRate;
    
    phasePosition += phaseIncrement;    //increase value of phasePosition by phaseIncrement.
    
    if (phasePosition > twoPi)
        phasePosition -= twoPi;     //if phasePosition is greater than twoPi, then subtract twoPi from the value, to ensure it stays in the 0 to 2π range.
    
    
    float out = sin(phasePosition);     //Use the sin() function to convert the current phase value into the sine of that value. Assign this result to both the left and right output using the pointers.
    
    return out * amplitude.get();
}