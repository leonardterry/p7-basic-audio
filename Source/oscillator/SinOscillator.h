//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 03/11/2016.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include "../../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>



class SinOscillator

{
public:
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Constructor */
    SinOscillator();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Destructor */
    ~SinOscillator();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Calculates the next output sample */
    float getSample();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Sets Oscillator Amplitude */
    void setAmplitude(float newAmplitude);
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Sets Oscillator Frequency */
    void setFrequency(float newFrequency);
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Sets Sample Rate, to avoid glitches */
    void setSampleRate(int sampleRate);
    
private:
    Atomic<float> frequency;
    Atomic<float> amplitude;
    float phasePosition;
    int sampleRate;
};

#endif /* SinOscillator_h */