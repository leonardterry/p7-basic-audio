/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//=============================================================================='
MainComponent::MainComponent (Audio& a) : audio (a) //CONSTRUCTOR
                                        //Thread("CounterThread")

{
    
    setSize (500, 400);
    
    counterButtonStartStop.setButtonText("Counter Button Start/Stop");
    counterButtonStartStop.setClickingTogglesState(true);
    addAndMakeVisible(counterButtonStartStop);
    counterButtonStartStop.addListener(this);

    counter.setListener(this);
    
    tempoSlider.setSliderStyle(juce::Slider::IncDecButtons);
    tempoSlider.setRange(60.0, 200.0, 1.0);
    tempoSlider.setValue(100);
    addAndMakeVisible(tempoSlider);
    tempoSlider.addListener(this);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
MainComponent::~MainComponent() //DESTRUCTOR
{
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MainComponent::resized() //CHANGE SIZE
{
//    gainSlider.setBounds(10, 10, 100, getHeight()-20);
    counterButtonStartStop.setBounds(10, 10, getWidth()-20, 100);
    tempoSlider.setBounds(10, 120, getWidth()-20, 100);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MainComponent::sliderValueChanged (Slider* slider)
{
    if (slider == &gainSlider){
        std::cout << "gainSlider: " << gainSlider.getValue() << std::endl;
        //audio.setGain(gainSlider.getValue());
    }
    
    if (slider == &tempoSlider){
        std::cout << "tempoSlider: " << tempoSlider.getValue() << std::endl;
        counter.setTempo(tempoSlider.getValue());
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MainComponent::buttonClicked (Button* button)
{
    // Counter Button Start/Stop
    if (button == &counterButtonStartStop)
    {
      
        DBG("counter button pressed");
        std::cout << "counter button state: " << counterButtonStartStop.getToggleState() << std::endl;
        
        //Conditional statement to check thread is not already running
        if (counter.isRunning() == false)
            counter.startCounterThread();
        else
            counter.stopCounterThread();
        
        if (counterButtonStartStop.getToggleState() == false)
            counter.stopCounterThread();
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MainComponent::counterChanged(const unsigned int counterValue)
{
    std::cout << "Counter Value: " << counterValue << std::endl;
    
    audio.beep();
   
}



//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

